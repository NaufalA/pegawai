<?php

namespace Database\Seeders;

use App\Models\Pegawai;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class PegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Pegawai::create([
            'NIP' => '197506082009031004',
            'nama_lengkap' => 'Yemmie Hendrawan Putra',
            'no_telp' => '08123456789',
            'jenis_pegawai' => 'PNS',
            'pangkat' => 'Penata Muda',
            'role' => 'admin',
            'jabatan' => 'Kepala Sub Bagian Peraturan Perundang - undangan',
            'password' => Hash::make('admin'),
        ]);

        Pegawai::create([
            'NIP' => '196909211990031010',
            'nama_lengkap' => 'Drs. SAYID MUHDAR',
            'no_telp' => '08123456789',
            'jenis_pegawai' => 'PNS',
            'pangkat' => 'Penata Muda',
            'jabatan' => 'Kepala Sub Bagian Peraturan Perundang - undangan',
            'password' => Hash::make('hendra'),
        ]);

        Pegawai::create([
            'NIP' => '196302091993031008',
            'nama_lengkap' => 'Ir.PANTI SUHARTONO',
            'no_telp' => '08123456789',
            'jenis_pegawai' => 'PNS',
            'pangkat' => 'Penata Muda',
            'jabatan' => 'Kepala Sub Bagian Peraturan Perundang - undangan',
            'password' => Hash::make('hendra'),
        ]);
    }
}