<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pegawais', function (Blueprint $table) {
            $table->id();
            // $table->string('id_asisten');
            // $table->string('id_skpd');
            // $table->string('id_bag_peg');
            // $table->string('id_induk_bagian');
            // $table->string('id_atasan');
            // $table->string('durasi_tanggal');
            // $table->string('unit_kerja');
            // $table->date('tanggal');
            // $table->string('foto');
            $table->string('NIP');
            $table->string('nama_lengkap');
            $table->string('no_telp');
            $table->enum('jenis_pegawai', ['PNS', 'NABAN']);
            $table->string('pangkat');
            $table->string('jabatan');
            $table->string('password');
            $table->enum('status_jabatan', ['0', '1', '2']);
            $table->enum('role', ['admin', 'user'])->default('user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pegawais');
    }
};
