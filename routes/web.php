<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\KegiatanController;
use App\Http\Controllers\PegawaiController;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/



Route::middleware('guest')->group(function () {
    Route::get('login', [AuthController::class, 'index'])->name('login.index');
    Route::post('login', [AuthController::class, 'login'])->name('login');
});

Route::middleware('admin')->group(function () {
    Route::get('/pegawai', function () {
        return view('pegawai.index');
    })->name('pegawai');

    Route::resource('pegawaiAjax', PegawaiController::class)->except('create', 'show');
});

Route::middleware('auth')->group(function () {
    Route::get('/', function () {
        return view('home');
    });

    Route::get('kegiatan', function () {
        return view('kegiatan.index');
    })->name('kegiatan');

    Route::resource('kegiatanAjax', KegiatanController::class)->except('create', 'show');

    Route::post('logout', [AuthController::class, 'logout'])->name('logout');
});
