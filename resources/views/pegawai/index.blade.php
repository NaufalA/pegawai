@extends('layouts.app')

@section('title', 'Pegawai')

@section('judul', 'Data Pegawai')

@section('main')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary" id="addPegawai">+
                            add data</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="pegawaiTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Nip</th>
                                    <th>No Telp</th>
                                    <th>Pangkat</th>
                                    <th>Jabatan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="pegawaiModal" tabindex="-1" role="dialog" aria-labelledby="pegawaiModalTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="alert alert-danger d-none"></div>
                    <div class="alert alert-success d-none"></div>
                    <div class="card-body">
                        {{-- <input type="text" hidden id="id_asisten" value="1">
                                <input type="text" hidden id="id_skpd" value="1">
                                <input type="text" hidden id="id_bag_peg" value="1">
                                <input type="text" hidden id="id_induk_bagian" value="1">
                                <input type="text" hidden id="id_atasan" value="1">
                                <input type="text" hidden id="jenis_pegawai" value="PNS">
                                <input type="text" hidden id="unit_kerja" value="0">
                                <input type="text" hidden id="tanggal" value="2023-09-14">
                                <input type="text" hidden id="foto" value="default.jpg">
                                <input type="text" hidden id="durasi_tanggal" value="2023-09-14"> --}}

                        <div class="form-group">
                            <label for="namaLengkap">Nama</label>
                            <input type="text" class="form-control" id="namaLengkap" placeholder="Masukkan Nama"
                                name="namaLengkap">
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" placeholder="Password"
                                name="password">

                        </div>

                        <div class="form-group">
                            <label for="NIP">NIP</label>
                            <input type="NIP" class="form-control" id="NIP" placeholder="Masukkan NIP"
                                name="NIP">
                        </div>

                        <div class="form-group">
                            <label for="notelp">No Telp</label>
                            <input type="notelp" class="form-control" id="notelp" placeholder="Masukkan No Telp"
                                name="notelp">
                        </div>

                        <div class="form-group">
                            <label for="pangkat">Pangkat</label>
                            <input type="pangkat" class="form-control" id="pangkat" placeholder="Masukkan pangkat"
                                name="pangkat">
                        </div>

                        <div class="form-group">
                            <label for="jabatan">Jabatan</label>
                            <input type="jabatan" class="form-control" id="jabatan" placeholder="Masukkan jabatan"
                                name="jabatan">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="closeModalPegawaiButton"
                        data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="buttonSimpanPegawai">Simpan</button>
                </div>
            </div>
        </div>
    </div>
@endsection
