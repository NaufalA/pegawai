<script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#pegawaiTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "pegawaiAjax",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'namaLengkap',
                    name: 'Nama',
                },
                {
                    data: 'NIP',
                    name: 'NIP',
                },
                {
                    data: 'notelp',
                    name: 'No Telp',
                },
                {
                    data: 'pangkat',
                    name: 'Pangkat',
                },
                {
                    data: 'jabatan',
                    name: 'Jabatan',
                },
                {
                    data: 'action',
                    name: 'action'
                }
            ]
        });
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('body').on('click', '#addPegawai', function(e) {
        e.preventDefault();
        $('#pegawaiModal').modal('show');

        $('#buttonSimpanPegawai').off('click');

        $('#buttonSimpanPegawai').click(function() {
            createPegawaiData();
        })
    });

    $('body').on('click', '#editPegawaiButton', function(e) {
        var id = $(this).data('id');
        $.ajax({
            url: 'pegawaiAjax/' + id + '/edit',
            type: 'GET',
            success: function(response) {
                $('#pegawaiModal').modal('show');
                $('#NIP').val(response.result.NIP);
                $('#namaLengkap').val(response.result.namaLengkap);
                $('#notelp').val(response.result.notelp);
                $('#jenis_pegawai').val(response.result.jenis_pegawai);
                $('#pangkat').val(response.result.pangkat);
                $('#jabatan').val(response.result.jabatan);
                // $('#password').val(response.result.password);
                console.log(response.result);

                $('#buttonSimpanPegawai').off('click');

                $('#buttonSimpanPegawai').click(function() {
                    createPegawaiData(id);
                });
            }
        });
    });

    $('body').on('click', '#deletePegawaiButton', function(e) {
        if (confirm('Yakin mau hapus data ini?') == true) {
            var id = $(this).data('id');
            $.ajax({
                url: 'pegawaiAjax/' + id,
                type: 'DELETE',
            });
            $('#pegawaiTable').DataTable().ajax.reload();
        }
    });


    function createPegawaiData(id = '') {
        if (id == '') {
            var var_url = 'pegawaiAjax';
            var var_type = 'POST';
        } else {
            var var_url = 'pegawaiAjax/' + id;
            var var_type = 'PUT';
        }
        $.ajax({
            url: var_url,
            type: var_type,
            data: {
                NIP: $('#NIP').val(),
                namaLengkap: $('#namaLengkap').val(),
                notelp: $('#notelp').val(),
                jenis_pegawai: $('#jenis_pegawai').val(),
                pangkat: $('#pangkat').val(),
                jabatan: $('#jabatan').val(),
                password: $('#password').val(),
            },
            success: function(response) {
                $('.is-invalid').removeClass('is-invalid');
                $('.invalid-feedback').remove();

                if (response.errors) {
                    console.log(response.errors);
                    // Jika terdapat kesalahan dalam respons
                    var errorMessages = response.errors;

                    $.each(errorMessages, function(field, error) {
                        // Menambahkan pesan kesalahan setelah elemen input dengan nama yang sesuai
                        var $input = $('input[name="' + field + '"]');
                        $input.addClass('is-invalid');

                        var errorMessageHTML =
                            '<span class="invalid-feedback" role="alert">' +
                            error + '</span>';
                        $input.after(errorMessageHTML);
                    });
                } else {
                    // Jika tidak ada kesalahan, reload tabel Anda
                    clearPegawaiModal();
                    $('#pegawaiTable').DataTable().ajax.reload();
                }
            }
        })

    }

    $(document).on('click', function(e) {
        if ($(e.target).hasClass('modal')) {
            clearPegawaiModal();
        }
    });

    $('#closeModalPegawaiButton').click(function() {
        clearPegawaiModal();
    });

    function clearPegawaiModal() {
        $('#pegawaiModal').modal('hide');

        $('#NIP').val('');
        $('#namaLengkap').val('');
        $('#notelp').val('');
        $('#jenis_pegawai').val('');
        $('#pangkat').val('');
        $('#jabatan').val('');
        $('#password').val('');

        $('.is-invalid').removeClass('is-invalid');
        $('.invalid-feedback').remove();
    }
</script>





{{-- // id_asisten: $('#id_asisten').val(),
// id_skpd: $('#id_skpd').val(),
// id_bag_peg: $('#id_bag_peg').val(),
// id_induk_bagian: $('#id_induk_bagian').val(),
// id_atasan: $('#id_atasan').val(),
// durasi_tanggal: $('#durasi_tanggal').val()
// tanggal: $('#tanggal').val(),
// unit_kerja: $('#unit_kerja').val(), --}}
