<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body {
            background: url('/background.jpg') no-repeat center center fixed;
            background-size: cover;
        }

        .login-box {
            margin-top: 100px;
            margin-right: 100px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.4);
            background-color: #fff;
            padding: 20px;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-md-6">
                <div class="login-box">
                    <h4 class="mb-4">Login</h4>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <label for="NIP">NIP</label>
                            <input type="text" class="form-control @error('NIP') is-invalid @enderror" id="NIP"
                                name="NIP" placeholder="Enter NIP" value="{{ old('NIP') }}">
                            @error('NIP')
                                <span class="invalid-feedback" role="alert">
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                id="password" name="password" placeholder="Enter password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>

</html>
