@extends('layouts.app')

@section('title', 'Pegawai')

@section('judul', 'Data Pegawai')

@section('main')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <a class="btn btn-primary" id="addKegiatan">+
                            add data</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="kegiatanTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kegiatan</th>
                                    <th>Tahun</th>
                                    <th>Pegawai</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="kegiatanModal" tabindex="-1" role="dialog" aria-labelledby="kegiatanModalTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="kegiatan">Kegiatan</label>
                            <input type="text" class="form-control" id="kegiatan"
                                placeholder="Masukkan Kegiatan" name="kegiatan">
                        </div>

                        <div class="form-group">
                            <label for="tahun">Tahun</label>
                            <input type="tahun" class="form-control" id="tahun" placeholder="Masukkan Tahun"
                                name="tahun">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="closeModalKegiatanButton"
                        data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="buttonSimpanKegiatan">Simpan</button>
                </div>
            </div>
        </div>
    </div>
@endsection
