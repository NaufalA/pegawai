<script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#kegiatanTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "kegiatanAjax",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'kegiatan',
                    name: 'kegiatan',
                },
                {
                    data: 'tahun',
                    name: 'tahun',
                },
                {
                    data: 'id_pegawai',
                    name: 'pegawai',
                },
                {
                    data: 'action',
                    name: 'action'
                }
            ]
        });
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('body').on('click', '#addKegiatan', function(e) {
        e.preventDefault();
        $('#kegiatanModal').modal('show');

        $('#buttonSimpanPegawai').off('click');

        $('#buttonSimpanKegiatan').click(function() {
            createKegiatanData();
        })
    });

    $('body').on('click', '#editKegiatanButton', function(e) {
        var id = $(this).data('id');
        $.ajax({
            url: 'kegiatanAjax/' + id + '/edit',
            type: 'GET',
            success: function(response) {
                $('#kegiatanModal').modal('show');
                $('#kegiatan').val(response.result.kegiatan);
                $('#tahun').val(response.result.tahun);
                console.log(response.result);

                $('#buttonSimpanPegawai').off('click');

                $('#buttonSimpanKegiatan').click(function() {
                    createKegiatanData(id);
                });
            }
        });
    });

    $('body').on('click', '#deleteKegiatanButton', function(e) {
        if (confirm('Yakin mau hapus data ini?') == true) {
            var id = $(this).data('id');
            $.ajax({
                url: 'kegiatanAjax/' + id,
                type: 'DELETE',
            });
            $('#kegiatanTable').DataTable().ajax.reload();
        }
    });

    function createKegiatanData(id = '') {
        if (id == '') {
            var var_url = 'kegiatanAjax';
            var var_type = 'POST';
        } else {
            var var_url = 'kegiatanAjax/' + id;
            var var_type = 'PUT';
        }
        $.ajax({
            url: var_url,
            type: var_type,
            data: {
                kegiatan: $('#kegiatan').val(),
                tahun: $('#tahun').val(),
            },
            success: function(response) {
                $('.is-invalid').removeClass('is-invalid');
                $('.invalid-feedback').remove();

                if (response.errors) {
                    console.log(response.errors);
                    // Jika terdapat kesalahan dalam respons
                    var errorMessages = response.errors;

                    $.each(errorMessages, function(field, error) {
                        // Menambahkan pesan kesalahan setelah elemen input dengan nama yang sesuai
                        var $input = $('input[name="' + field + '"]');
                        $input.addClass('is-invalid');

                        var errorMessageHTML =
                            '<span class="invalid-feedback" role="alert">' +
                            error + '</span>';
                        $input.after(errorMessageHTML);
                    });
                } else {
                    // Jika tidak ada kesalahan, reload tabel Anda
                    clearModalKegiatan();
                    $('#kegiatanTable').DataTable().ajax.reload();
                }
            }
        })

    }

    $(document).on('click', function(e) {
        if ($(e.target).hasClass('modal')) {
            clearModalKegiatan();
        }
    });

    $('#closeModalKegiatanButton').click(function() {
        clearModalKegiatan();
    });

    function clearModalKegiatan() {
        $('#kegiatanModal').modal('hide');

        $('#kegiatan').val('');
        $('#tahun').val('');

        $('.is-invalid').removeClass('is-invalid');
        $('.invalid-feedback').remove();
    }
</script>
