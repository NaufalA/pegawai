<?php

namespace App\Http\Controllers;

use App\Models\Kegiatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class KegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Kegiatan::where('id_pegawai', auth()->user()->id_pegawai);
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                return view('kegiatan.action', compact('data'));
            })
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = Validator::make($request->all(), [
            'kegiatan' => 'required',
            'tahun' => 'required',
        ]);

        if ($data->fails()) {
            return response()->json(['errors' => $data->errors()]);
        } else {
            // $data = $request->all();
            $data = [
                'id_pegawai' => auth()->user()->id_pegawai,
                'kegiatan' => $request->kegiatan,
                'tahun' => $request->tahun,
            ];

            Kegiatan::create($data);

            return response()->json(['success' => "Berhasil menyimpan data"]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = Kegiatan::where('id_kegiatan', $id)->first();
        return response()->json(['result' => $data]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $data = Validator::make($request->all(), [
            'kegiatan' => 'required',
            'tahun' => 'required',
        ]);

        if ($data->fails()) {
            return response()->json(['errors' => $data->errors()]);
        } else {
            $data = $request->all();

            Kegiatan::where('id_kegiatan', $id)->update($data);

            return response()->json(['success' => "Berhasil menyimpan data"]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Kegiatan::where('id_kegiatan', $id)->delete();
    }
}
