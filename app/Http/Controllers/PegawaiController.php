<?php

namespace App\Http\Controllers;

use App\Models\Pegawai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Pegawai::all();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                return view('pegawai.action', compact('data'));
            })
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = Validator::make($request->all(), [
            'NIP' => 'required',
            'namaLengkap' => 'required',
            'notelp' => 'required|numeric',
            'pangkat' => 'required',
            'jabatan' => 'required',
            'password' => 'required',
        ]);

        if ($data->fails()) {
            return response()->json(['errors' => $data->errors()]);
        } else {
            $data = $request->all();
            $data['password'] = Hash::make($data['password']);

            Pegawai::create($data);

            return response()->json(['success' => "Berhasil menyimpan data"]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = Pegawai::where('id_pegawai', $id)->first();
        return response()->json(['result' => $data]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $data = Validator::make($request->all(), [
            'NIP' => 'required',
            'namaLengkap' => 'required',
            'notelp' => 'required|numeric',
            'pangkat' => 'required',
            'jabatan' => 'required',
        ]);

        if ($data->fails()) {
            return response()->json(['errors' => $data->errors()]);
        } else {
            $data = $request->all();
            if (!empty($data['password'])) {
                $data['password'] = Hash::make($data['password']);
                Pegawai::where('id_pegawai', $id)->update($data);
                return response()->json(['success' => "Berhasil menyimpan data"]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Pegawai::where('id_pegawai', $id)->delete();
    }
}
