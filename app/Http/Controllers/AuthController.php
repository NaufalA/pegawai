<?php

namespace App\Http\Controllers;

use App\Models\Pegawai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login'); // Ganti 'auth.login' dengan nama view login yang sesuai dengan struktur Anda
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'NIP' => ['required'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->intended('dashboard');
        }

        return back()->withErrors([
            'NIP' => 'The provided credentials do not match our records.',
        ])->onlyInput('NIP');
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }

    // public function login(Request $request)
    // {
    //     $request->validate([
    //         'NIP' => 'required',
    //         'password' => 'required',
    //     ]);

    //     $pegawai = Pegawai::where('NIP', $request->NIP)->first();

    //     if (!$pegawai) {
    //         return redirect()->back()->withErrors(['NIP' => 'NIP tidak terdaftar']);
    //     }

    //     if (!Hash::check($request->password, $pegawai->password)) {
    //         return redirect()->back()->withErrors(['password' => 'password salah.']);
    //     }

    //     Auth::login($pegawai);

    //     return redirect()->route('dashboard');
    // }
}