<?php

namespace App\Providers;

use App\Models\Pegawai;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Gate::define('admin', function (Pegawai $pegawai) {
            return $pegawai->role === 'admin';
        });
    }
}
