<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kegiatan extends Model
{
    use HasFactory;

    protected $table = 'master_skp';

    protected $primaryKey = 'id_pegawai';

    protected $attributes = [
        'id_bagian' => '1',
        'id_subag' => '1',
    ];

    protected $fillable = ['id_pegawai', 'tahun', 'kegiatan'];

    public $timestamps = false;
}
