<?php

namespace App\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model implements Authenticatable
{
    use HasFactory;

    public function getAuthIdentifierName()
    {
        return 'NIP'; // Sesuaikan dengan kolom NIP dalam tabel 'pegawai'
    }

    public function getAuthIdentifier()
    {
        return $this->attributes['NIP']; // Sesuaikan dengan kolom NIP dalam tabel 'pegawai'
    }

    public function getAuthPassword()
    {
        return $this->attributes['password']; // Sesuaikan dengan kolom password dalam tabel 'pegawai'
    }

    public function getRememberToken()
    {
        return null; // Jika Anda tidak menggunakan fitur "remember me", kembalikan null
    }

    public function setRememberToken($value)
    {
        // Jika Anda tidak menggunakan fitur "remember me", tidak perlu melakukan apa-apa
    }

    public function getRememberTokenName()
    {
        return null; // Jika Anda tidak menggunakan fitur "remember me", kembalikan null
    }

    protected $table = 'pegawai';

    protected $primaryKey = 'id_pegawai';

    protected $attributes = [
        'id_asisten' => '1',
        'id_skpd' => '1',
        'id_bag_peg' => '1',
        'id_induk_bagian' => '1',
        'id_atasan' => '1',
        'durasi_tgl' => '7',
        'tanggal' => '2023-09-14',
        'unitKerja' => '1',
        'foto' => '1',
        'jenis_pegawai' => 'PNS',
        'status_jabatan' => '2',
        'role' => '',
    ];

    protected $fillable = ['NIP', 'namaLengkap', 'notelp', 'jenis_pegawai', 'pangkat', 'jabatan', 'password'];

    public $timestamps = false;
}
